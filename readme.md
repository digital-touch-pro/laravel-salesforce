# Laravel Salesforce

A Laravel library for Salesforce built on top of omniphx/forrest.

## Navigation

- [Installation](#installation)
- [Usage](#usage)
- [Methods](#methods)
- [Links](#links)

## Installation

Install the package:

```shell
composer require digitaltouch/laravel-salesforce
```

Publish the configuration file (it will publish a `config/forrest.php` file):

```shell
php artisan vendor:publish
```

Extend the model:

```php
<?php

namespace App\Models;

use Digitaltouch\Salesforce\Models\Model as BaseModel;

abstract class Model extends BaseModel
{
    //
}
```

## Usage

###### Facade

To access the facade (it will be automatically registered) methods, simply define `use` statement:

```php
use Salesforce;
```

###### Model

To define the Salesforce object associated with the model:

```php
/**
 * The table associated with the model.
 *
 * @var string
 */
protected $table = 'ObjectName__c';
```

To define the list of object fields:

```php
/**
 * The attributes that are mass assignable.
 *
 * @var string[]
 */
protected $fillable = [
    'ObjectFieldA'    => 'fieldAliasA',
    'ObjectFieldB__c' => 'fieldAliasB',
];
```

To define the list of readonly fields (fields that are used for select and hidden for save):

```php
/**
 * The attributes that should be hidden for save.
 *
 * @var array
 */
protected $readonly = [
    'fieldAliasA',
];
```

To define the list of virtual fields (fields that are hidden for select and for save):

```php
/**
 * The attributes that should be hidden for select and save.
 *
 * @var array
 */
protected $virtual = [
    'fieldAliasB',
];
```

## Methods

- Facade
    - `Salesforce::fetch($query)` - fetch a single record
    - `Salesforce::fetchAll($query)` - fetch all records
    - `Salesforce::insert($object, $record)` - insert a single record
    - `Salesforce::update($object, $field, $record)` - update a single record
    - `Salesforce::upsert($object, $field, $record)` - update or insert a single record
    - `Salesforce::delete($object, $field, $id)` - delete a single record
    - `Salesforce::massInsert($object, $records)` - mass insert records
    - `Salesforce::massUpdate($object, $field, $records)` - mass update records
    - `Salesforce::massUpsert($object, $field, $records)` - mass update or insert records
    - `Salesforce::massDelete($object, $field, $ids)` - mass delete records

- Model
    - `Model::all()` - get all models
    - `Model::findAll($where?, $orderBy?)` - find models by optional where clause
    - `Model::find($id)` - find a model by the identifier
    - `Model::findOrFail($id)` - find a model by the identifier, if no result is found, an exception will be thrown
    - `$model->getId()` - get the model identifier

## Casting

All native Laravel casts should work as expected. Additional casts:

- `multi_picklist` - casts values of multi-select picklist from/to array

## Links

- [Salesforce](https://salesforce.com/)
    - [REST API](https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/intro_what_is_rest_api.htm)
    - [SOQL](https://developer.salesforce.com/docs/atlas.en-us.soql_sosl.meta/soql_sosl/sforce_api_calls_soql.htm)
    - [SOSL](https://developer.salesforce.com/docs/atlas.en-us.soql_sosl.meta/soql_sosl/sforce_api_calls_sosl.htm)
- [omniphx/forrest](https://github.com/omniphx/forrest)
    - [Configuration](https://github.com/omniphx/forrest#configuration)
    - [Application Setup](https://github.com/omniphx/forrest#setting-up-a-connected-app)
