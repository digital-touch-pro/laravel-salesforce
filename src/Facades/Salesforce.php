<?php

namespace Digitaltouch\Salesforce\Facades;

use Illuminate\Support\Str;
use Omniphx\Forrest\Providers\Laravel\Facades\Forrest as Facade;

class Salesforce extends Facade
{
    /**
     * The "PATCH" method name.
     *
     * @const string
     */
    const METHOD_PATCH = 'PATCH';

    /**
     * The "POST" method name.
     *
     * @const string
     */
    const METHOD_POST = 'POST';

    /**
     * The "DELETE" method name.
     *
     * @const string
     */
    const METHOD_DELETE = 'DELETE';

    /**
     * The maximum amount of sub-requests per request.
     *
     * @const int
     */
    const MAX_SUBREQUESTS = 25;

    /**
     * Fetch a single record.
     *
     * @param string $query
     *
     * @return array
     */
    public static function fetch(string $query)
    {
        $response = static::query($query);
        $records = static::extractRecords($response);

        return reset($records);
    }

    /**
     * Fetch all records.
     *
     * @param string $query
     *
     * @return array
     */
    public static function fetchAll(string $query)
    {
        $response = static::query($query);
        $records = static::extractRecords($response);

        while (!empty($response['nextRecordsUrl']))
        {
            $response = static::next($response['nextRecordsUrl']);
            $records = array_merge($records, static::extractRecords($response));
        }

        return $records;
    }

    /**
     * Insert a single record.
     *
     * @param string $object
     * @param array  $record
     *
     * @return array
     */
    public static function insert(string $object, array $record)
    {
        return static::sendRequest(static::METHOD_POST, $object, $record);
    }

    /**
     * Update a single record.
     *
     * @param string $object
     * @param string $field
     * @param array  $record
     *
     * @return array
     */
    public static function update(string $object, string $field, array $record)
    {
        $endpoint = static::prepareEndpoint([
            $object,
            $field,
            $record[$field],
        ]);

        unset($record[$field]);

        return static::sendRequest(static::METHOD_PATCH, $endpoint, $record);
    }

    /**
     * Update or insert a single record.
     *
     * @param string $object
     * @param string $field
     * @param array  $record
     *
     * @return array
     */
    public static function upsert(string $object, string $field, array $record)
    {
        $method = empty($record[$field])
            ? static::METHOD_POST
            : static::METHOD_PATCH;

        $endpoint = static::prepareEndpoint([
            $object,
            $field,
            $record[$field],
        ]);

        unset($record[$field]);

        return static::sendRequest($method, $endpoint, $record);
    }

    /**
     * Delete a single record.
     *
     * @param string $object
     * @param string $field
     * @param string $id
     *
     * @return array
     */
    public static function delete(string $object, string $field, string $id)
    {
        $endpoint = static::prepareEndpoint([
            $object,
            $field,
            $id,
        ]);

        return static::sendRequest(static::METHOD_DELETE, $endpoint);
    }

    /**
     * Mass insert records.
     *
     * @param string $object
     * @param array  $records
     *
     * @return array
     */
    public static function massInsert(string $object, array $records)
    {
        $requests = [];
        $resources = static::resources();

        foreach ($records as $index => $record)
        {
            $reference = static::prepareReference($object, $index);

            $endpoint = static::prepareEndpoint([
                $resources['sobjects'],
                $object,
            ]);

            $requests[] = static::prepareCompositeRequest($reference, static::METHOD_POST, $endpoint, $record);
        }

        return static::sendCompositeRequest($requests);
    }

    /**
     * Mass update records.
     *
     * @param string $object
     * @param string $field
     * @param array  $records
     *
     * @return array
     */
    public static function massUpdate(string $object, string $field, array $records)
    {
        $requests = [];
        $resources = static::resources();

        foreach ($records as $index => $record)
        {
            $reference = static::prepareReference($object, $index);

            $endpoint = static::prepareEndpoint([
                $resources['sobjects'],
                $object,
                $field,
                $record[$field],
            ]);

            unset($record[$field]);

            $requests[] = static::prepareCompositeRequest($reference, static::METHOD_PATCH, $endpoint, $record);
        }

        return static::sendCompositeRequest($requests);
    }

    /**
     * Mass update or insert records.
     *
     * @param string $object
     * @param string $field
     * @param array  $records
     *
     * @return array
     */
    public static function massUpsert(string $object, string $field, array $records)
    {
        $requests = [];
        $resources = static::resources();

        foreach ($records as $index => $record)
        {
            $reference = static::prepareReference($object, $index);

            $endpoint = static::prepareEndpoint([
                $resources['sobjects'],
                $object,
                $field,
                $record[$field],
            ]);

            unset($record[$field]);

            $requests[] = static::prepareCompositeRequest($reference, static::METHOD_PATCH, $endpoint, $record);
        }

        return static::sendCompositeRequest($requests);
    }

    /**
     * Mass delete records.
     *
     * @param string $object
     * @param string $field
     * @param array  $ids
     *
     * @return array
     */
    public static function massDelete(string $object, string $field, array $ids)
    {
        $requests = [];
        $resources = static::resources();

        foreach ($ids as $index => $id)
        {
            $reference = static::prepareReference($object, $index);

            $endpoint = static::prepareEndpoint([
                $resources['sobjects'],
                $object,
                $field,
                $id,
            ]);

            $requests[] = static::prepareCompositeRequest($reference, static::METHOD_DELETE, $endpoint);
        }

        return static::sendCompositeRequest($requests);
    }

    /**
     * Extract response records.
     *
     * @param array $response
     *
     * @return array
     */
    private static function extractRecords(array $response)
    {
        $records = $response['records'] ?? [];

        foreach ($records as &$record)
        {
            foreach ($record as $field => &$value)
            {
                if (Str::endsWith($field, '__r') && isset($value['records']))
                {
                    $value = static::extractRecords($value);
                }
            }
        }

        return $records;
    }

    /**
     * Prepare a request endpoint.
     *
     * @param array $parts
     *
     * @return string
     */
    private static function prepareEndpoint(array $parts)
    {
        return implode('/', array_filter($parts));
    }

    /**
     * Prepare a request body.
     *
     * @param array $record
     *
     * @return array
     */
    private static function prepareBody(array $record)
    {
        foreach ($record as &$value)
        {
            if (is_array($value) && isset($value[0]))
            {
                $value = array_filter($value);
                $value = implode(';', $value);
            }
        }

        return $record;
    }

    /**
     * Prepare a record reference.
     *
     * @param string $object
     * @param string $suffix
     *
     * @return string
     */
    private static function prepareReference(string $object, string $suffix)
    {
        return 'reference_' . $object . '_' . $suffix;
    }

    /**
     * Prepare a composite request.
     *
     * @param string     $reference
     * @param string     $method
     * @param string     $endpoint
     * @param array|null $record
     *
     * @return array
     */
    private static function prepareCompositeRequest(string $reference, string $method, string $endpoint, array $record = [])
    {
        return [
            'referenceId' => $reference,
            'method'      => $method,
            'url'         => $endpoint,
            'body'        => static::prepareBody($record),
        ];
    }

    /**
     * Send a request.
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $record
     *
     * @return array
     */
    private static function sendRequest(string $method, string $endpoint, array $record = [])
    {
        return static::sobjects($endpoint, [
            'method' => $method,
            'body'   => static::prepareBody($record),
        ]);
    }

    /**
     * Send a composite request.
     *
     * @param array $requests
     *
     * @return array
     */
    private static function sendCompositeRequest(array $requests)
    {
        $responses = [];
        $resources = static::resources();

        for ($i = 0, $n = ceil(sizeof($requests) / static::MAX_SUBREQUESTS); $i <= $n; $i++)
        {
            $response = static::post($resources['composite'], [
                'compositeRequest' => array_slice($requests, $i * static::MAX_SUBREQUESTS, static::MAX_SUBREQUESTS)
            ]);

            $responses = array_merge($responses, $response['compositeResponse']);
        }

        return $responses;
    }
}
