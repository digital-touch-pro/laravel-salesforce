<?php

namespace Digitaltouch\Salesforce\Models;

use DateTimeInterface;
use InvalidArgumentException;
use Digitaltouch\Salesforce\Facades\Salesforce;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;

abstract class Model extends BaseModel
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that should be hidden for save.
     *
     * @var array
     */
    protected $readonly = [];

    /**
     * The attributes that should be hidden for select and save.
     *
     * @var array
     */
    protected $virtual = [];

    /**
     * Get all of the models from the database.
     *
     * @param array|mixed $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function all($columns = ['*'])
    {
        return static::findAll();
    }

    /**
     * Find a model by its primary key.
     *
     * @param mixed $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
     */
    public static function find($id, $columns = ['*'])
    {
        if (is_array($id))
        {
            return static::findMany($id, $columns);
        }

        $instance = new static();

        return static::findAll($instance->getKeyNameForSave() . ' = \'' . $id . '\'')->first();
    }

    /**
     * Find multiple models by their primary keys.
     *
     * @param \Illuminate\Contracts\Support\Arrayable|array $ids
     * @param array                                         $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function findMany($ids, $columns = ['*'])
    {
        $instance = new static();

        if (empty($ids))
        {
            return $instance->newCollection();
        }

        return static::findAll($instance->getKeyNameForSave() . ' IN(\'' . implode('\', \'', $ids) . '\')');
    }

    /**
     * Find a model by its primary key or throw an exception.
     *
     * @param mixed $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static|static[]
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public static function findOrFail($id, $columns = ['*'])
    {
        $result = static::find($id, $columns);

        if (is_array($id))
        {
            if (count($result) === count(array_unique($id)))
            {
                return $result;
            }
        }
        elseif (!is_null($result))
        {
            return $result;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class(new static), $id
        );
    }

    /**
     * Find multiple models by where clause.
     *
     * @param string|null $where
     * @param string|null $orderBy
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function findAll(string $where = null, string $orderBy = null)
    {
        $instance = new static();

        $query = 'SELECT ' . implode(', ', $instance->getFieldsForSelect()) . '
                  FROM ' . $instance->getTable();

        if ($where)
        {
            $query .= ' WHERE ' . $where;
        }

        if ($orderBy)
        {
            $query .= ' ORDER BY ' . $orderBy;
        }

        $results = Salesforce::fetchAll($query);
        $models = [];

        foreach ($results as $result)
        {
            $models[] = new static($instance->replaceFieldsWithAliases($result));
        }

        return $instance->newCollection($models);
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getId()
    {
        return $this->getAttribute(
            $this->getKeyName()
        );
    }

    /**
     * Reload the current model instance with fresh attributes from the database.
     *
     * @return $this
     */
    public function refresh()
    {
        if (!$this->getId())
        {
            return $this;
        }

        $this->setRawAttributes(
            static::findOrFail($this->getId())->attributes
        );

        $this->syncOriginal();

        return $this;
    }

    /**
     * Update the model in the database.
     *
     * @param array $attributes
     * @param array $options
     *
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        if (!$this->getId())
        {
            return false;
        }

        return $this->fill($attributes)->save($options);
    }

    /**
     * Save the model to the database.
     *
     * @param array $options
     *
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->fireModelEvent('saving') === false)
        {
            return false;
        }

        if ($this->getId())
        {
            $saved = $this->isDirty() ? $this->executeUpdate() : true;
        }
        else
        {
            $saved = $this->executeInsert();
        }

        if ($saved)
        {
            $this->finishSave($options);
        }

        return $saved;
    }

    /**
     * Get the primary key for save.
     *
     * @return string
     */
    public function getKeyNameForSave()
    {
        return $this->getFieldFromAlias(
            $this->getKeyName()
        );
    }

    /**
     * Replace field names with aliases.
     *
     * @param array $fields
     *
     * @return array
     */
    public function replaceFieldsWithAliases(array $fields)
    {
        $return = [];

        foreach ($this->getFillable() as $field => $alias)
        {
            if (isset($fields[$field]))
            {
                $return[$alias] = $fields[$field];
            }
        }

        return $return;
    }

    /**
     * Determine if the attribute is readonly.
     *
     * @param string $key
     *
     * @return bool
     */
    protected function isReadonly(string $key)
    {
        return in_array($key, $this->getReadonly());
    }

    /**
     * Get a list of the model's readonly attributes.
     *
     * @return array
     */
    protected function getReadonly()
    {
        return $this->readonly;
    }

    /**
     * Determine if the attribute is virtual.
     *
     * @param string $key
     *
     * @return bool
     */
    protected function isVirtual(string $key)
    {
        return in_array($key, $this->getVirtual());
    }

    /**
     * Get a list of the model's virtual attributes.
     *
     * @return array
     */
    protected function getVirtual()
    {
        return $this->virtual;
    }

    /**
     * Get a list of fields for select.
     *
     * @return array
     */
    protected function getFieldsForSelect()
    {
        $fields = [];

        foreach ($this->getFillable() as $field => $alias)
        {
            if (!$this->isVirtual($alias))
            {
                $fields[] = $field;
            }
        }

        return $fields;
    }

    /**
     * Get a list of fields for save.
     *
     * @return array
     */
    protected function getFieldsForSave()
    {
        $fields = [];

        foreach ($this->getFillable() as $field => $alias)
        {
            if (!$this->isReadonly($alias) && !$this->isVirtual($alias))
            {
                $value = $this->getAttributeValue($alias);

                if ($this->hasCast($alias))
                {
                    $value = $this->castAttributeForSave($alias, $value);
                }

                $fields[$field] = $value;
            }
        }

        return $fields;
    }

    /**
     * Get a field name of the given alias.
     *
     * @param string $alias
     *
     * @return string|null
     */
    protected function getFieldFromAlias(string $alias)
    {
        $fields = array_flip($this->getFillable());

        return $fields[$alias] ?? null;
    }

    /**
     * Execute a model update operation.
     *
     * @return bool
     */
    protected function executeUpdate()
    {
        if ($this->fireModelEvent('updating') === false)
        {
            return false;
        }

        Salesforce::upsert(
            $this->getTable(),
            $this->getKeyNameForSave(),
            $this->getFieldsForSave()
        );

        $this->syncChanges();

        $this->fireModelEvent('updated', false);

        return true;
    }

    /**
     * Execute a model insert operation.
     *
     * @return bool
     */
    protected function executeInsert()
    {
        if ($this->fireModelEvent('creating') === false)
        {
            return false;
        }

        if (empty($this->getAttributes()))
        {
            return true;
        }

        $response = Salesforce::upsert(
            $this->getTable(),
            $this->getKeyNameForSave(),
            $this->getFieldsForSave()
        );

        if (!$id = $response['id'] ?? null)
        {
            return false;
        }

        $this->setAttribute($this->getKeyName(), $id);

        $this->exists = true;
        $this->wasRecentlyCreated = true;

        $this->fireModelEvent('created', false);

        return true;
    }

    /**
     * Cast an attribute to a native PHP type.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        $castType = $this->getCastType($key);

        switch ($castType)
        {
            case 'bool':
            case 'boolean':
                return (bool)$value;
        }

        if (is_null($value))
        {
            return $value;
        }

        switch ($castType)
        {
            case 'multi_picklist':
                return explode(';', $value);
        }

        return parent::castAttribute($key, $value);
    }

    /**
     * Cast an attribute for save.
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return mixed
     */
    protected function castAttributeForSave($key, $value)
    {
        if (is_null($value))
        {
            return $value;
        }

        switch ($this->getCastType($key))
        {
            case 'date':
                return $value->toDateString();
            case 'datetime':
            case 'custom_datetime':
                return $value->toIso8601ZuluString();
            case 'multi_picklist':
                return implode(';', $value);
        }

        return $value;
    }

    /**
     * Return a timestamp as DateTime object.
     *
     * @param mixed $value
     *
     * @return \Illuminate\Support\Carbon
     */
    protected function asDateTime($value)
    {
        try
        {
            $date = parent::asDateTime($value);
        }
        catch (InvalidArgumentException $e)
        {
            $date = false;
        }

        return $date ?: Carbon::parse($value);
    }

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param \DateTimeInterface $date
     *
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return Carbon::instance($date)->toIso8601ZuluString();
    }
}
